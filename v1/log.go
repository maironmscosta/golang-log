package v1

import (
	"encoding/json"
	"log"
	"time"
)

const (
	LogLevelInfo    = "INFO"
	LogLevelWarning = "WARNING"
	LogLevelError   = "ERROR"
)

type Logging struct {
	Logger         *log.Logger
	mappingMessage map[string]interface{}
}

func NewLogging(logger *log.Logger) Logging {

	return Logging{
		Logger: logger,
	}
}

func (l Logging) Info(message string, v ...interface{}) {

	l.mappingMessage = interfaceArrayToMap(v)
	l.message(LogLevelInfo, message)

}

func (l Logging) Warning(message string, v ...interface{}) {

	l.mappingMessage = interfaceArrayToMap(v)
	l.message(LogLevelWarning, message)

}

func (l Logging) Error(message string, v ...interface{}) {

	l.mappingMessage = interfaceArrayToMap(v)
	l.message(LogLevelError, message)

}

func (l Logging) message(level string, message string) {

	l.mappingMessage["level"] = level
	l.mappingMessage["message"] = message
	l.mappingMessage["date"] = time.Now().Format(time.RFC3339Nano)
	marshal, _ := json.Marshal(l.mappingMessage)
	l.Logger.Println(string(marshal))
}

func (l Logging) Base(v ...interface{}) Logging {

	l.mappingMessage = interfaceArrayToMap(v)
	return l
}

func interfaceArrayToMap(v ...interface{}) map[string]interface{} {

	mapping := make(map[string]interface{})
	if len(v) > 0 {
		m := v[0].([]interface{})
		//fmt.Println(fmt.Sprintf("m: %v", m))
		for i := 0; i < len(m); i++ {
			if i%2 == 1 {
				mapping[m[i-1].(string)] = m[i]
			}
		}
	}

	return mapping
}
