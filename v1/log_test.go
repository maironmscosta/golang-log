package v1

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"testing"
)

//var logging Logging

func TestLogging_Info(t *testing.T) {

	tests := []struct {
		Name         string
		Message      string
		ParamName    string
		ParamArg     interface{}
		ExpectResult string
	}{
		{
			Name:      "Log Info: success",
			Message:   "First Test",
			ParamName: "number",
			ParamArg:  01,
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			logger := log.New(os.Stdout, "", log.Flags())

			log := NewLogging(logger)
			log.Info(test.Message, test.ParamName, test.ParamArg)

		})

	}
}

func TestLogger(t *testing.T) {

	logTest("testando", "num1", "1", "num2", 2, "num3", 3)

}

func logTest(message string, v ...interface{}) {

	mapping := map[string]interface{}{
		"message": message,
	}
	for i := 0; i < len(v); i++ {
		fmt.Println(fmt.Sprintf("k: %v - v: %v", i, v[i]))
		if i%2 == 1 {
			mapping[v[i-1].(string)] = v[i]
		}
	}

	marshal, _ := json.Marshal(mapping)
	logger := log.New(os.Stdout, "", log.Flags())
	logger.Println(string(marshal))

}
